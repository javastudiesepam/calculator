package com.epam.app;


import com.epam.app.calculator.Calculator;

public class TestCalculator {
  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_RED = "\u001B[31m";
  private static final String ANSI_GREEN = "\u001B[32m";

  public static void main(String[] args) {
    testCalculator();
  }

  private static void testCalculator() {
    boolean assumption;
    {
      String expression = "2+2";
      String it = "Should correctly evaluate " + expression;

      Calculator calc = new Calculator();
      assumption = calc.evaluate(expression) == 4;

      printResult(assumption, it);
    }
    {
      String expression = "2+2*2";
      String it = "Should correctly evaluate " + expression;

      Calculator calc = new Calculator();
      assumption = calc.evaluate(expression) == 6;

      printResult(assumption, it);
    }
    {
      String expression = "2+2*2+2";
      String it = "Should correctly evaluate " + expression;

      Calculator calc = new Calculator();
      assumption = calc.evaluate(expression) == 8;

      printResult(assumption, it);
    }
    {
      String expression = "2+2*2+2+150/3";
      String it = "Should correctly evaluate " + expression;

      Calculator calc = new Calculator();
      assumption = calc.evaluate(expression) == 58;

      printResult(assumption, it);
    }
  }

  private static void printResult(Boolean assumption, String it) {
    if (assumption) {
      System.out.println(ANSI_GREEN + "pass " + ANSI_RESET + it);
    } else {
      System.out.println(ANSI_RED + "fail " + ANSI_RESET + it);
    }
  }
}
