package com.epam.app.calculator;

import com.epam.app.expressions.Expression;
import com.epam.app.parsing.MathParser;
import com.epam.app.parsing.ParsingUtils;

import java.util.Queue;


public class Calculator {
  public int evaluate(String expression) {
    String[] expressionArray = ParsingUtils.tokenize(expression);

    //infix to Postfix
    Queue<String> pfExpr = ParsingUtils.infixToPostFixArr(expressionArray);

    //build the Binary Tree
    Expression rootNode = MathParser.buildTree(pfExpr);

    //Evaluate the tree
    return rootNode.evaluate();
  }
}
