package com.epam.app.expressions;

public interface Expression {
  int evaluate();
}
