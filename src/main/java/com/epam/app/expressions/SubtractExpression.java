package com.epam.app.expressions;

public class SubtractExpression extends NonTerminalExpression {
  public int evaluate() {
    return getLeftNode().evaluate() - getRightNode().evaluate();
  }

  public SubtractExpression(Expression l, Expression r) {
    super(l, r);
  }
}