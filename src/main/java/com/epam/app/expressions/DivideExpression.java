package com.epam.app.expressions;

public class DivideExpression extends NonTerminalExpression {
  public int evaluate() {
    return getLeftNode().evaluate() / getRightNode().evaluate();
  }

  public DivideExpression(Expression l, Expression r) {
    super(l, r);
  }
}
