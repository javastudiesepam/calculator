package com.epam.app.expressions;


public class AddExpression extends NonTerminalExpression {
  public int evaluate() {
    return getLeftNode().evaluate() + getRightNode().evaluate();
  }

  public AddExpression(Expression l, Expression r) {
    super(l, r);
  }
}
