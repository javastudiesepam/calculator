package com.epam.app.expressions;

public class MultiplyExpression extends NonTerminalExpression {
  public int evaluate() {
    return getLeftNode().evaluate() * getRightNode().evaluate();
  }

  public MultiplyExpression(Expression l, Expression r) {
    super(l, r);
  }
}
