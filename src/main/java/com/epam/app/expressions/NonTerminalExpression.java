package com.epam.app.expressions;

public abstract class NonTerminalExpression implements Expression {
  private Expression leftNode;
  private Expression rightNode;

  NonTerminalExpression(Expression leftNode, Expression rightNode) {
    this.leftNode = leftNode;
    this.rightNode = rightNode;
  }

  Expression getLeftNode() {
    return leftNode;
  }

  Expression getRightNode() {
    return rightNode;
  }
}
