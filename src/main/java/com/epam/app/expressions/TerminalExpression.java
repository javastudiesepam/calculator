package com.epam.app.expressions;


public class TerminalExpression implements Expression {
  private Integer value;

  public TerminalExpression(String value) {
    this.value = Integer.parseInt(value);
  }

  public int evaluate() {
    return value;
  }
}
