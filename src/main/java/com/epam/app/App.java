package com.epam.app;

import com.epam.app.calculator.Calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class App {
    public static void main(String[] args) {
        System.out.println("Enter an expression: ");
        System.out.print(" ");
        String expression = "";

        try {
            expression = new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Calculator calc = new Calculator();

        System.out.println(" Result = " + calc.evaluate(expression));
    }
}
