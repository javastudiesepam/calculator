package com.epam.app.parsing;

import java.util.*;
import java.util.regex.Pattern;

public class ParsingUtils {
  static final String PLUS_SIGN = "+";
  static final String MINUS_SIGN = "-";
  static final String MULTIPLY_SIGN = "*";
  static final String DIVIDE_SIGN = "/";
  private static final String tokenizeMathExpressionPattern = "(?=[+-/*])|(?<=[+-/*])";

  public static String[] tokenize(String expression) {
     return expression.split(tokenizeMathExpressionPattern);
  }

  static boolean isOperator(String str) {
    return str.matches("[" + Pattern.quote(PLUS_SIGN + MINUS_SIGN + MULTIPLY_SIGN + DIVIDE_SIGN) + "]");
  }

  public static Queue<String> infixToPostFixArr(String[] expressionArr) {

    Stack<String> stack = new Stack<>();
    Queue<String> postfixExpression = new LinkedList<>();

    String lastStoredOperator;

    for (String currChar : expressionArr) {

      if (isOperator(currChar)) {
        if (!stack.isEmpty()) {
          lastStoredOperator = stack.pop();

          int priority1 = getPriority(lastStoredOperator);
          int priority2 = getPriority(currChar);

          if (priority1 >= priority2) {
            postfixExpression.add(lastStoredOperator);
            priority1 = 0;

            if (!stack.isEmpty()) {
              lastStoredOperator = stack.pop();
              priority1 = getPriority(lastStoredOperator);
            }
          }
          if (priority1 != 0) {
            stack.push(lastStoredOperator);
          }
        }
        stack.push(currChar);
      } else {
        postfixExpression.add(currChar);
      }
    }

    while (!stack.isEmpty()) {
      postfixExpression.add(stack.pop());
    }

    return postfixExpression;
  }

  private static Integer getPriority(String val) {
    Map<String, Integer> priorities = new HashMap<>();

    priorities.put(PLUS_SIGN, 1);
    priorities.put(MINUS_SIGN, 1);
    priorities.put(MULTIPLY_SIGN, 2);
    priorities.put(DIVIDE_SIGN, 2);

    return priorities.get(val);
  }
}
