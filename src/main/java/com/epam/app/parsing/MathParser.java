package com.epam.app.parsing;


import com.epam.app.expressions.*;

import java.util.Queue;
import java.util.Stack;

public class MathParser {
  public static Expression buildTree(Queue<String> expr) {
    Stack<Expression> expressionStack = new Stack<>();

    for (String element: expr) {

      if (!ParsingUtils.isOperator(element)) {
        Expression e = new TerminalExpression(element);
        expressionStack.push(e);
      } else {
        Expression right = expressionStack.pop();
        Expression left = expressionStack.pop();
        expressionStack.push(getNonTerminalExpression(element, left, right));
      }
    }

    return expressionStack.pop();
  }

  private static NonTerminalExpression getNonTerminalExpression(String operation, Expression l, Expression r) {
    switch (operation) {
      case ParsingUtils.PLUS_SIGN:
        return new AddExpression(l, r);
      case ParsingUtils.MINUS_SIGN:
        return new SubtractExpression(l, r);
      case ParsingUtils.MULTIPLY_SIGN:
        return new MultiplyExpression(l, r);
      case ParsingUtils.DIVIDE_SIGN:
        return new DivideExpression(l, r);
    }
    return null;
  }
}
